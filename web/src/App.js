import React from 'react';

import { useWallet } from './hooks/use-wallet';
import { useBalanceState } from './hooks/use-balance-state';
import { useContract } from './hooks/use-contract';

export default function App() {
  const { initialized, address, error: walletError, loading: walletLoading, connect: connectToWallet } = useWallet();
  const {
    storage,
    error: contractError,
    loading: contractLoading,
    contract,
    operationsCount,
    connect: connectToContract,
    increaseOperationsCount,
  } = useContract();
  const { balance, error: balanceError, loading: balanceLoading } = useBalanceState(address, operationsCount);

  const [operationLoading, setOperationLoading] = React.useState(false);
  const [operationError, setOperationError] = React.useState('');

  return (
    <div className="app">
      {initialized && (
        <>
          <div>Counter: {contractLoading ? 'Loading' : storage}</div>
          <div>Address: {walletLoading ? 'Loading' : address}</div>
          <div>Balance: {balanceLoading ? 'Loading' : balance}</div>
        </>
      )}
      {walletError && <div>Wallet Error: {walletError}</div>}
      {balanceError && <div>Balance Error: {balanceError}</div>}
      {contractError && <div>Contract Error: {contractError}</div>}
      {operationError && <div>Operation Error: {operationError}</div>}
      {initialized ? (
        <>
          <button onClick={increase} disabled={contractLoading || operationLoading}>
            Increase
          </button>
          <button onClick={decrease} disabled={contractLoading || operationLoading}>
            Decrease
          </button>
          {operationLoading && `Loading...`}
        </>
      ) : (
        <button onClick={connect}>Connect</button>
      )}
    </div>
  );

  async function connect() {
    await connectToWallet();
    await connectToContract();
  }

  async function increase() {
    if (!contract) {
      return;
    }
    setOperationLoading(true);
    try {
      const op = await contract.methods.increment(1).send();
      await op.confirmation();
      increaseOperationsCount();
    } catch (error) {
      setOperationError(error.message);
    }
    setOperationLoading(false);
  }

  async function decrease() {
    if (!contract) {
      return;
    }
    setOperationLoading(true);
    try {
      const op = await contract.methods.decrement(1).send();
      await op.confirmation();
      increaseOperationsCount();
    } catch (error) {
      setOperationError(error.message);
    }
    setOperationLoading(false);
  }
}
