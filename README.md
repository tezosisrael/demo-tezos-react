# Tezos counter app

Note: this post is based on @claudebarde's [post](https://medium.com/coinmonks/build-a-tezos-dapp-using-taquito-and-the-beacon-sdk-692d7dc822aa) and is using truffle base example for the Counter contract. If you just want to learn the basics, I suggest following @claudebarde's post. I'm creating almost the same app, with react and truffle.

1. bootstrap:

- create a folder for the project

```sh
mkdir tezos-counter-app
cd tezos-counter-app
```

- run the following:

```sh
yarn create react-app web # this will create the folder for your app
cd web
yarn add @taquito/beacon-wallet @taquito/taquito

cd ../
yarn global add truffle@tezos
mkdir contracts
cd contracts
truffle unbox tezos-example
```

2. what?

- `yarn create react-app web` bootstraps a react app with all the defaults.
- `yarn add @taquito/beacon-wallet @taquito/taquito` - install taquito dependencies
- `yarn global add truffle@tezos` installs truffle, you can skip this step if it's already installed.
  Notes:

1. you need to check that you have the tezos version of truffle, if you don't, you'll need to uninstall and install this version.
2. I just reinstalled my computer and I had a few problems. First, truffle installation includes compiling c++ code, so you're expected to have it installed. Second, although it says node 12 is supported it failed on my machine so I used node v10.

- `truffle unbox tezos-example`
  This bootstraps the tezos-example of truffle. This includes simple contracts, migrations, and tests. We will use the Counter contract for this tutorial.

Let's also install the [beacon chrome extension](https://chrome.google.com/webstore/detail/beacon-extension/gpfndedineagiepkpinficbcbbgjoenn)

3. configure contract deployment.

As @claudebarde's tutorial uses carthegent we will use the same for deploying our contracts. Open tezos-counter-app folder using your favorite editor.

Check [truffle](https://www.trufflesuite.com/docs/tezos/truffle/quickstart) for more explanation about the following commands.

Using https://faucet.tzalpha.net/ generate a faucet account and save it into the contracts folder as `faucet.json`.

replace `truffle-config.js` content with the following:

```js
const { mnemonic, secret, password, email } = require('./faucet.json');

module.exports = {
  networks: {
    development: {
      host: 'https://carthagenet.smartpy.io',
      port: 443,
      network_id: '*',
      secret,
      mnemonic,
      password,
      email,
      type: 'tezos',
    },
  },
};
```

run `truffle migrate` inside the contracts folder. I forgot to save truffle-config.js and it failed, so be sure that you've saved the file.

This command deploys the contracts to the blockchain (carthagenet) and sets their initial storage (check the contracts and migrations folders to see some code). When it succeeds you can see the contract's addresses in your terminal and the build folder (JSON file for each contract). Copy the counter contract account and go to [BCD](https://better-call.dev/) to see the contract. Whenever we do any operation on this contract, you can see it here.

You can actually use BCD app to interact with the contract already.

run `truffle test` to see the tests running and then check BCD to see the tests interactions with the contract.

OK, since we're not developing a contract here (that's a part I'm still learning), the next part is the main one, developing the app.

3. Let's start with client-side development

In your terminal go into `web` folder and run `yarn start`. This will start the react development server, so any change we do in the app code will reflect almost immediately in the browser. when `yarn start` finishes the initial build you'll be presented with a URL, probably `localhost:3000`. Open it in your browser and let's start.

Open `App.js` and replace its content with

```jsx
import React from 'react';

function App() {
  return <div className="App"></div>;
}

export default App;
```

You can see in the browser that the initial app was replaced with an empty file

Let's add the main ingredients for the app (it won't be beautiful, sorry)

replace `<div className="App"></div>`
with

```jsx
<div className="app">
  <div>Counter: </div>
  <div>Address: </div>
  <div>Balance: </div>
  <div>Wallet Error: </div>
  <div>Balance Error: </div>
  <div>Contract Error: </div>
  <div>Operation Error: </div>
  <button>Increase</button>
  <button>Decrease</button>
  <button>Connect</button>
</div>
```

4. Connect to the wallet

We will create a few react hooks for this app, so let's create a new folder `hooks` and create a new file inside it `use-wallet.js`.

VSCode tip: right-click the src folder and choose to create a file, when asked for a name for the file write `hooks/use-wallet.js`, this will create the folder and the file.

write inside `use-wallet` the following:

```js
import { useState } from 'react';
import { Tezos } from '@taquito/taquito';
import { BeaconWallet } from '@taquito/beacon-wallet';

export function useWallet() {
  const [initialized, setInit] = useState(false);
  const [address, setAddress] = useState('');
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  return { initialized, address, error, loading, connect };

  function connect() {}
}
```

for now, it doesn't do anything, but provide us with the API we want. this hook will tell us when the wallet is initialized, it will provide the address of the account, an error is something is wrong and a connect function. We've also imported Tezos and BeaconWallet to connect and ask for permission to use a wallet. For more info about hooks read [react's docs](https://reactjs.org/docs/hooks-intro.html)

connect will call taquito to configure the account and network we want to use, and then ask beacon to get permissions from the user to connect. Finally, it will return the address.

replace `connect` with the following

```js
async function connect() {
  setLoading(true);
  try {
    const { address } = await initWallet();
    setInit(true);
    setAddress(address);
  } catch (error) {
    setError(error.message);
  } finally {
    setLoading(false);
  }
}

async function initWallet() {
  Tezos.setProvider({ rpc: 'https://carthagenet.SmartPy.io' });
  const options = {
    name: 'Tezos counter app',
  };
  const wallet = new BeaconWallet(options);
  const network = { type: 'carthagenet' };
  await wallet.requestPermissions({ network });
  Tezos.setWalletProvider(wallet);

  const address = wallet.permissions.address;
  return { address };
}
```

Actually, there's no need to separate connect to two functions, but I prefer it like that, so we can see what happens in the react side (`connect`) and taquito side `initWallet`. As this is more an overview for how to connect @claudebarde with react, let's go to his post for an explanation about [initWallet](https://medium.com/coinmonks/build-a-tezos-dapp-using-taquito-and-the-beacon-sdk-692d7dc822aa#ecb3)

and to use it, let's import it to `App.js`:

```jsx
import React from 'react';

import { useWallet } from './hooks/use-wallet';

export default function App() {
  const { initialized, address, error: walletError, loading: walletLoading, connect: connectToWallet } = useWallet();

  return (
    <div className="app">
      {initialized && (
        <>
          <div>Counter: </div>
          <div>Address: {walletLoading ? 'Loading' : address}</div>
          <div>Balance: </div>
        </>
      )}
      {walletError && <div>Wallet Error: {walletError}</div>}
      <div>Balance Error: </div>
      <div>Contract Error: </div>
      <div>Operation Error: </div>
      {initialized ? (
        <>
          <button>Increase</button>
          <button>Decrease</button>
        </>
      ) : (
        <button onClick={connectToWallet}>Connect</button>
      )}
    </div>
  );
}
```

You can see I added an import of the useWallet function from `use-wallet` and called it on the first line of our component. These values will change when we try to connect. `initialized` will be true and `address` will be assigned with the wallet address if we succeed to connect, and `error` will have a value if we failed to connect. I renamed `connect` to `connectToWallet` and `error` to `walletError` because we will have more of these soon.

So I can hide and show different parts of the app based on if the wallet is initialized or not. For example, I'll show the connect button if it's not, and the increase/decrease buttons if it is connected.

6. Let's play

Open your app and press connect, you'll see the following screen:

![Request permission](request-permission.png)

Press confirm, and you'll be asked to pair your account (if you haven't before - if you did, make sure this account is a carthagenet account). Let's create a local account by enabling Developer mode and choosing "Setup local secret" (You might need to scroll down a bit). You'll see the account details:

![Account created](account-created.png)

Close the window and press connect again, this time you'll see the option to grant permissions, press confirm, the window will close and a popup (inside the app) will appear with the text "Permission Granted". Going to the explorer won't give us any info because our account isn't revealed yet (meaning, the blockchain doesn't know about it yet). But when we will close the popup we will see the account address, and the buttons are replaced with "Increase"/"Decrease".

7. Let's activate the account

When we first create the account, it's not online yet, thus we need to do an operation on-chain to activate it and reveal it, the easiest would be to send it some tez. You can do that if you have another carthagenet account, or otherwise you can use [@tezos_faucet_bot](https://web.telegram.org/#/im?p=@tezos_faucet_bot) on telegram. In the bot's options choose "Get Coins" and send it to your newly created address.

Now your account should be activated.

8. Checking the Balance

To check the balance we can do two things: Either use the wallet hook or create a new hook. I like the second option because then we can use this hook to get balances for other addresses.

Create a new file `hooks/use-balance-state.js`

```js
import {useState} from 'react'

export function useBalanceState(address = '') {
  const [balance, setBalance] = useState(0);
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  ...

  return {balance, error, loading};
}
```

as before I first show the hook API, we only need the balance, error, and a loading flag, no functions. This hook will update automatically when the address is changed.

Let's first connect it to the app so we can see it update automatically.

Import useBalanceState into App.js and show the balance and error when available:

`App.js`:

```jsx
export default function App() {
  const { initialized, address, error: walletError, connect: connectToWallet } = useWallet();
  const { balance, error: balanceError, loading: balanceLoading } = useBalanceState(address);

  return (
    <div className="app">
      {initialized && (
        <>
          <div>Counter: </div>
          <div>Address: {address}</div>
          <div>Balance: {balanceLoading ? 'Loading' : balance}</div>
        </>
      )}
      {walletError && <div>Wallet Error: {walletError}</div>}
      {balanceError && <div>Balance Error: {balanceError}</div>}
      <div>Contract Error: </div>
      <div>Operation Error: </div>
      {initialized ? (
        <>
          <button>Increase</button>
          <button>Decrease</button>
        </>
      ) : (
        <button onClick={connectToWallet}>Connect</button>
      )}
    </div>
  );
}
```

Nothing will change in the app except that the balance is 0 and "Balance Error" disappeared.

Let's add loading of the balance inside `use-balance-state.js`:

```js
import { useState, useEffect } from 'react';
import { Tezos } from '@taquito/taquito';

export function useBalanceState(address = '') {
  const [balance, setBalance] = useState(0);
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    loadBalance(address);
  }, [address]);

  return { balance, error, loading };

  async function loadBalance(address) {
    if (!address) {
      return;
    }
    try {
      setLoading(true);
      const balance = await Tezos.tz.getBalance(address);
      setBalance(balance / 10 ** 6);
    } catch (e) {
      setError(e.message);
    } finally {
      setLoading(false);
    }
  }
}
```

React doesn't like using of async functions as an effect so we create a loadBalance function that manages the loading of the balance. `useEffect` basically calls the provided function any time one of the values in the second array parameter changes, in our case, every time the address is changing.

`loadBalance` will use taquito to load the balance of the provided address. taquito returns a value in mutez, so we divide it by 1000000 to get the amount in tez.

Check your app, press connect (this time no confirmation is needed), and see how we first see the address and balance is loading, then we see the amount @tezos_faucet_bot sent us.

Next, get some contract data.

9. Connect to contract:

If you didn't copy the contract address, go into `contracts/build/contracts/Counter.json`. You'll see something like this:

```json
{
  "contractName": "Counter",
  "abi": [],
  "michelson": "michelson-code",
  "sourcePath": "/some/path",
  "compiler": {
    "name": "ligo",
    "version": "next"
  },
  "networks": {
    "NetXjD3HPJJjmcd": {
      "events": {},
      "links": {},
      "address": "KT1RSvh5UgLa39NM2SDF6gZRSqagFjJ2LGG9",
      "transactionHash": "oovupVBfaYHPABEKyHfLZyYoqmCAAbNAuxvGFBeCUfjAEVEbm4R"
    }
  },
  "schemaVersion": "3.2.0-tezos.1",
  "updatedAt": "2020-07-05T13:04:54.168Z",
  "networkType": "tezos"
}
```

Copy the address value from this file, this is `YOUR_CONTRACT_ADDRESS`.

Let's create another hook `useContract` that will connect us to the counter contract.

`use-contract.js`

```js
import { useState } from 'react';
import { Tezos } from '@taquito/taquito';

const CONTRACT_ADDRESS = 'YOUR_CONTRACT_ADDRESS';

export function useContract() {
  const [contract, setContract] = useState(null);
  const [error, setError] = useState('');
  const [storage, setStorage] = useState(0);
  const [loading, setLoading] = useState(false);

  return { contract, error, storage, loading, connect };

  async function connect() {
    setLoading(true);
    try {
      const contractInstance = await Tezos.wallet.at(CONTRACT_ADDRESS);
      setContract(contractInstance);
      await loadStorage(contractInstance);
    } catch (err) {
      setError(err.message);
    } finally {
      setLoading(false);
    }
  }

  async function loadStorage(contract) {
    if (!contract) {
      return;
    }
    try {
      const storage = await contract.storage();
      setStorage(Number(storage));
    } catch (e) {
      setError(e.message);
    }
  }
}
```

replace `YOUR_CONTRACT_ADDRESS` with the contract address you copied.

What we do here, is to use taquito to load the contract at the provided address, and then load its storage. We load the storage in this hook and not in another hook because storage is dependent on the contract.

Let's connect it to see how it works.

`App.js`:

```jsx
export default function App() {
  const { initialized, address, error: walletError, loading: walletLoading, connect: connectToWallet } = useWallet();
  const { balance, error: balanceError, loading: balanceLoading } = useBalanceState(address);
  const { storage, error: contractError, loading: contractLoading, connect: connectToContract } = useContract();

  return (
    <div className="app">
      {initialized && (
        <>
          <div>Counter: {contractLoading ? 'Loading' : storage}</div>
          <div>Address: {walletLoading ? 'Loading' : address}</div>
          <div>Balance: {balanceLoading ? 'Loading' : balance}</div>
        </>
      )}
      {walletError && <div>Wallet Error: {walletError}</div>}
      {balanceError && <div>Balance Error: {balanceError}</div>}
      {contractError && <div>Contract Error: {contractError}</div>}
      <div>Operation Error: </div>
      {initialized ? (
        <>
          <button>Increase</button>
          <button>Decrease</button>
        </>
      ) : (
        <button onClick={connect}>Connect</button>
      )}
    </div>
  );

  async function connect() {
    await connectToWallet();
    await connectToContract();
  }
}
```

If you review the code (or even better, write it in your App.js file), you'll see that we imported the contract hook and used it.
The main difference from what we saw until now, is the `connect` function. Since connectToContract also needs to be called when connecting, we call them both in this function. The order is important because all the settings (network, etc.) are set in the connectToWallet. We might be able to call it in a different function, just to make the calls independent of each other.

10. Interact with the contract

We want the user to be able to increase and decrease the counter. The contract API lets its users increase and decrease any amount, but we will stick to 1.

Let's implement the increase function first. We will implement it inside the App component, although in a new hook or inside the contract hook is also possible.

bottom of App component (in `App.js` inside the `App` function)

```js
async function increase() {
  if (!contract) {
    return;
  }
  setOperationLoading(true);
  try {
    const op = await contract.methods.increment(1).send();
    await op.confirmation();
  } catch (error) {
    setOperationError(error.message);
  }
  setOperationLoading(false);
}
```

and change the increase button to

```jsx
<button onClick={increase}>Increase</button>
```

Note that increase calls `setOperationError` and `setOperationLoading`, so let's add them in the upper part of the App component:

```js
const [operationLoading, setOperationLoading] = React.useState(false);
const [operationError, setOperationError] = React.useState('');
```

and ofcourse, we want to show it in the app, so replace the buttons sections with this:

```jsx
<>
  <button onClick={increase} disabled={operationLoading}>
    Increase
  </button>
  <button disabled={operationLoading}>Decrease</button>
  {operationLoading && `Loading...`}
</>
```

Note how we set the buttons to be disabled when the operation is loading because we don't want the user to click a few times.

Let's see it happening. Go to the browser and click increase (connect again if needed). Since it's the first operation on the contract we need to reveal it on the chain, so you'll be asked to permit to operations. Once you agree, a beacon popup will be open, you can close it and see how the app is loading and the buttons are greyed out (disabled). Once the operation is confirmed on-chain, they will be enabled again.

What's wrong? The counter didn't change. If you refresh the page and reconnect, you'll see that it did increase and the account balance is a bit lower then what it was. Why didn't it update automatically? Because we didn't tell it to. Let's do that.

11. Auto-sync account balance and contract storage.

Ok, this is the tricky part, I'm pretty sure I've got a good solution, but if you think of something else, please let me know.

We want to update the balance and storage when an operation occurs. We can use `useEffect` for that.

Something like:

```js
useEffect(() => {
  updateBalance();
  updateStorage();
}, [opertaions]);
```

That's just pseudocode, as storage is in the contract, and balance is in the account, we will implement them in the different hooks.

Let's add a counter of the contract operations, this will be the variable through which we track that an operation has been done.

`useContract`:

```js
import { useState, useEffect } from 'react';
import { Tezos } from '@taquito/taquito';

const CONTRACT_ADDRESS = 'YOUR_CONTRACT_ADDRESS';

export function useContract() {
  const [contract, setContract] = useState(null);
  const [error, setError] = useState('');
  const [storage, setStorage] = useState(0);
  const [loading, setLoading] = useState(false);
  const [operationsCount, setOperationsCounter] = useState(0);

  useEffect(() => {
    loadStorage(contract);
  }, [contract, operationsCount]);

  return { contract, error, storage, loading, connect, increaseOperationsCount };

  function increaseOperationsCount() {
    setOperationsCounter(operationsCount + 1);
  }

  async function connect() {
    setLoading(true);
    try {
      const contractInstance = await Tezos.wallet.at(CONTRACT_ADDRESS);
      setContract(contractInstance);
      increaseOperationsCount();
    } catch (err) {
      setError(err.message);
    } finally {
      setLoading(false);
    }
  }

  async function loadStorage(contract) {
    if (!contract) {
      return;
    }
    try {
      setLoading(true);
      const storage = await contract.storage();
      setStorage(Number(storage));
    } catch (e) {
      setError(e.message);
    } finally {
      setLoading(false);
    }
  }
}
```

What did we change? We added the operations counter, and instead of loading the storage when the contract is loaded, we increase the operations counter. With that, we also added a `useEffect` that is dependent on the counter and contract and calls `loadStorage` whenever one of its dependencies change.

If you check your app now, it should act the same as before. The difference is that we expose `increaseOperationsCount` so we can call it inside `increase` after the confirmation. Let's do that. Add `increaseOperationsCount();` after `await op.confirmation();`.

Try your app again, this time the counter should increase by one, but the balance didn't. That's because we need to implement the same inside `useBalanceState`. If you look at your `useBalanceState` you'll see that we already have the `useEffect`, we just need to add a dependency on `operationsCount`, that means passing it into the hook, and adding it to the dependencies array:

```js
import { useState, useEffect } from 'react';
import { Tezos } from '@taquito/taquito';

export function useBalanceState(address = '', contractOperationsCount = 0) {
  const [balance, setBalance] = useState(0);
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    loadBalance(address);
  }, [address, contractOperationsCount]);

  return { balance, error, loading };

  async function loadBalance(address) {
    if (!address) {
      return;
    }
    try {
      setLoading(true);
      const balance = await Tezos.tz.getBalance(address);
      setBalance(balance / 10 ** 6);
    } catch (e) {
      setError(e.message);
    } finally {
      setLoading(false);
    }
  }
}
```

In App.js, we need to pass the operationsCount into `useBalanceState`, that means to move the call to this hook after the call to `useContract` (because `operationsCount` comes from `useContract`). Let's do that and see if it works.

The decrease function is almost the same as the increase, so I'll leave it for you.

12. Wrap up

This was just a short (planned to be short) review of how to create a simple react app that interacts with tezos. I ignored a lot of things here, like best practices, security, and more. I am still learning this stuff as I go.

A few things you can do:

- While writing this I understood that the contract interactions should also be in the contract hook, feel free to refactor and move them there.
- Implement decrease
- Let the user decide the amount to increase/decrease
- Connect automatically when the user opens the app
- Check the repo to see the whole code.
- There's another contract that comes with truffle examples (actually two, but the migrations contract is used behind the scenes), implement interaction with it.
- Write me some comments - here or on twitter to @chiptus
- read @claudebarde's [post](https://medium.com/coinmonks/build-a-tezos-dapp-using-taquito-and-the-beacon-sdk-692d7dc822aa) for more info about the implementation
