# Building a simple react dapp on the tezos blockchain - Part 2

This post follows my (last post)[./part1.md] where we bootstrapped a simple react app. In this post, we will connect our app to a wallet and show its balance.

This is part 2 of a series of posts about how to connect a react webapp to the tezos blockchain. For other posts see:

(1. Part 1 - Bootstrapping)[./part1.md]

2. Part 2 - Connect to a wallet
3. Part 3 - Connect and interact with a contract

## Connect to the wallet

We will create a few react hooks for this app, so let's create a new folder `hooks` and create a new file inside it `use-wallet.js`.

VSCode tip: right-click the src folder and choose to create a file, when asked for a name for the file write `hooks/use-wallet.js`, this will create the folder and the file.

write inside `use-wallet` the following:

```js
import { useState } from 'react';
import { Tezos } from '@taquito/taquito';
import { BeaconWallet } from '@taquito/beacon-wallet';

export function useWallet() {
  const [initialized, setInit] = useState(false);
  const [address, setAddress] = useState('');
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  return { initialized, address, error, loading, connect };

  function connect() {}
}
```

for now, it doesn't do anything, but provide us with the API we want. this hook will tell us when the wallet is initialized, it will provide the address of the account, an error is something is wrong and a connect function. We've also imported Tezos and BeaconWallet to connect and ask for permission to use a wallet. For more info about hooks read [react's docs](https://reactjs.org/docs/hooks-intro.html)

connect will call taquito to configure the account and network we want to use, and then ask beacon to get permissions from the user to connect. Finally, it will return the address.

replace `connect` with the following

```js
async function connect() {
  setLoading(true);
  try {
    const { address } = await initWallet();
    setInit(true);
    setAddress(address);
  } catch (error) {
    setError(error.message);
  } finally {
    setLoading(false);
  }
}

async function initWallet() {
  Tezos.setProvider({ rpc: 'https://carthagenet.SmartPy.io' });
  const options = {
    name: 'Tezos counter app',
  };
  const wallet = new BeaconWallet(options);
  const network = { type: 'carthagenet' };
  await wallet.requestPermissions({ network });
  Tezos.setWalletProvider(wallet);

  const address = wallet.permissions.address;
  return { address };
}
```

Actually, there's no need to separate connect to two functions, but I prefer it like that, so we can see what happens in the react side (`connect`) and taquito side `initWallet`. As this is more an overview for how to connect @claudebarde with react, let's go to his post for an explanation about [initWallet](https://medium.com/coinmonks/build-a-tezos-dapp-using-taquito-and-the-beacon-sdk-692d7dc822aa#ecb3)

and to use it, let's import it to `App.js`:

```jsx
import React from 'react';

import { useWallet } from './hooks/use-wallet';

export default function App() {
  const { initialized, address, error: walletError, loading: walletLoading, connect: connectToWallet } = useWallet();

  return (
    <div className="app">
      {initialized && (
        <>
          <div>Counter: </div>
          <div>Address: {walletLoading ? 'Loading' : address}</div>
          <div>Balance: </div>
        </>
      )}
      {walletError && <div>Wallet Error: {walletError}</div>}
      <div>Balance Error: </div>
      <div>Contract Error: </div>
      <div>Operation Error: </div>
      {initialized ? (
        <>
          <button>Increase</button>
          <button>Decrease</button>
        </>
      ) : (
        <button onClick={connectToWallet}>Connect</button>
      )}
    </div>
  );
}
```

You can see I added an import of the useWallet function from `use-wallet` and called it on the first line of our component. These values will change when we try to connect. `initialized` will be true and `address` will be assigned with the wallet address if we succeed to connect, and `error` will have a value if we failed to connect. I renamed `connect` to `connectToWallet` and `error` to `walletError` because we will have more of these soon.

So I can hide and show different parts of the app based on if the wallet is initialized or not. For example, I'll show the connect button if it's not, and the increase/decrease buttons if it is connected.

## Let's play

Open your app and press connect, you'll see the following screen:

![Request permission](request-permission.png)

Press confirm, and you'll be asked to pair your account (if you haven't before - if you did, make sure this account is a carthagenet account). Let's create a local account by enabling Developer mode and choosing "Setup local secret" (You might need to scroll down a bit). You'll see the account details:

![Account created](account-created.png)

Close the window and press connect again, this time you'll see the option to grant permissions, press confirm, the window will close and a popup (inside the app) will appear with the text "Permission Granted". Going to the explorer won't give us any info because our account isn't revealed yet (meaning, the blockchain doesn't know about it yet). But when we will close the popup we will see the account address, and the buttons are replaced with "Increase"/"Decrease".

## Let's activate the account

When we first create the account, it's not online yet, thus we need to do an operation on-chain to activate it and reveal it, the easiest would be to send it some tez. You can do that if you have another carthagenet account, or otherwise you can use [@tezos_faucet_bot](https://web.telegram.org/#/im?p=@tezos_faucet_bot) on telegram. In the bot's options choose "Get Coins" and send it to your newly created address.

Now your account should be activated.

## Checking the Balance

To check the balance we can do two things: Either use the wallet hook or create a new hook. I like the second option because then we can use this hook to get balances for other addresses.

Create a new file `hooks/use-balance-state.js`

```js
import {useState} from 'react'

export function useBalanceState(address = '') {
  const [balance, setBalance] = useState(0);
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  ...

  return {balance, error, loading};
}
```

as before I first show the hook API, we only need the balance, error, and a loading flag, no functions. This hook will update automatically when the address is changed.

Let's first connect it to the app so we can see it update automatically.

Import useBalanceState into App.js and show the balance and error when available:

`App.js`:

```jsx
export default function App() {
  const { initialized, address, error: walletError, connect: connectToWallet } = useWallet();
  const { balance, error: balanceError, loading: balanceLoading } = useBalanceState(address);

  return (
    <div className="app">
      {initialized && (
        <>
          <div>Counter: </div>
          <div>Address: {address}</div>
          <div>Balance: {balanceLoading ? 'Loading' : balance}</div>
        </>
      )}
      {walletError && <div>Wallet Error: {walletError}</div>}
      {balanceError && <div>Balance Error: {balanceError}</div>}
      <div>Contract Error: </div>
      <div>Operation Error: </div>
      {initialized ? (
        <>
          <button>Increase</button>
          <button>Decrease</button>
        </>
      ) : (
        <button onClick={connectToWallet}>Connect</button>
      )}
    </div>
  );
}
```

Nothing will change in the app except that the balance is 0 and "Balance Error" disappeared.

Let's add loading of the balance inside `use-balance-state.js`:

```js
import { useState, useEffect } from 'react';
import { Tezos } from '@taquito/taquito';

export function useBalanceState(address = '') {
  const [balance, setBalance] = useState(0);
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    loadBalance(address);
  }, [address]);

  return { balance, error, loading };

  async function loadBalance(address) {
    if (!address) {
      return;
    }
    try {
      setLoading(true);
      const balance = await Tezos.tz.getBalance(address);
      setBalance(balance / 10 ** 6);
    } catch (e) {
      setError(e.message);
    } finally {
      setLoading(false);
    }
  }
}
```

React doesn't like using of async functions as an effect so we create a loadBalance function that manages the loading of the balance. `useEffect` basically calls the provided function any time one of the values in the second array parameter changes, in our case, every time the address is changing.

`loadBalance` will use taquito to load the balance of the provided address. taquito returns a value in mutez, so we divide it by 1000000 to get the amount in tez.

Check your app, press connect (this time no confirmation is needed), and see how we first see the address and balance is loading, then we see the amount @tezos_faucet_bot sent us.

In the next post, we will connect to the contract and make some interactions.

For more stuff about tezos, go to our [blog](tezos.co.il)

