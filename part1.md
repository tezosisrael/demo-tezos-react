# Building a simple react dapp on the tezos blockchain - Part 1

This is part 1 of a series of posts about how to connect a react webapp to the tezos blockchain. For other posts see:

1. Part 1 - Bootstrapping
2. Part 2 - Connect to a wallet
3. Part 3 - Connect and interact with a contract

Note: this series is based on @claudebarde's [post](https://medium.com/coinmonks/build-a-tezos-dapp-using-taquito-and-the-beacon-sdk-692d7dc822aa) and is using truffle base example for the Counter contract. If you just want to learn the basics, I suggest following @claudebarde's post. I'm creating almost the same app, with react and truffle.

Complete code for this series is on [my repo](https://gitlab.com/chiptus/demo-tezos-react)

## Who am I?

My name is Chaim Lev-Ari, I'm a web developer. In the last 2 years, I started getting into Tezos. First, as a baker when I started BakeryIL, and these days I'm starting with smart contract development. These posts are describing my journey into the ecosystem.

## bootstrap:

- create a folder for the project

```sh
mkdir tezos-counter-app
cd tezos-counter-app
```

- run the following:

```sh
yarn create react-app web # this will create the folder for your app
cd web
yarn add @taquito/beacon-wallet @taquito/taquito

cd ../
yarn global add truffle@tezos
mkdir contracts
cd contracts
truffle unbox tezos-example
```

## what?

- `yarn create react-app web` bootstraps a react app with all the defaults.
- `yarn add @taquito/beacon-wallet @taquito/taquito` - install taquito dependencies
- `yarn global add truffle@tezos` installs truffle, you can skip this step if it's already installed.
  Notes:

1. you need to check that you have the tezos version of truffle, if you don't, you'll need to uninstall and install this version.
2. I just reinstalled my computer and I had a few problems. First, truffle installation includes compiling c++ code, so you're expected to have it installed. Second, although it says node 12 is supported it failed on my machine so I used node v10.

- `truffle unbox tezos-example`
  This bootstraps the tezos-example of truffle. This includes simple contracts, migrations, and tests. We will use the Counter contract for this tutorial.

Let's also install the [beacon chrome extension](https://chrome.google.com/webstore/detail/beacon-extension/gpfndedineagiepkpinficbcbbgjoenn)

## configure contract deployment.

As @claudebarde's tutorial uses carthegent we will use the same for deploying our contracts. Open tezos-counter-app folder using your favorite editor.

Check [truffle](https://www.trufflesuite.com/docs/tezos/truffle/quickstart) for more explanation about the following commands.

Using https://faucet.tzalpha.net/ generate a faucet account and save it into the contracts folder as `faucet.json`.

replace `truffle-config.js` content with the following:

```js
const { mnemonic, secret, password, email } = require('./faucet.json');

module.exports = {
  networks: {
    development: {
      host: 'https://carthagenet.smartpy.io',
      port: 443,
      network_id: '*',
      secret,
      mnemonic,
      password,
      email,
      type: 'tezos',
    },
  },
};
```

run `truffle migrate` inside the contracts folder. I forgot to save truffle-config.js and it failed, so be sure that you've saved the file.

This command deploys the contracts to the blockchain (carthagenet) and sets their initial storage (check the contracts and migrations folders to see some code). When it succeeds you can see the contract's addresses in your terminal and the build folder (JSON file for each contract). Copy the counter contract account and go to [BCD](https://better-call.dev/) to see the contract. Whenever we do any operation on this contract, you can see it here.

You can actually use BCD app to interact with the contract already.

run `truffle test` to see the tests running and then check BCD to see the tests interactions with the contract.

OK, since we're not developing a contract here (that's a part I'm still learning), the next part is the main one, developing the app.

## Let's start with client-side development

In your terminal go into `web` folder and run `yarn start`. This will start the react development server, so any change we do in the app code will reflect almost immediately in the browser. when `yarn start` finishes the initial build you'll be presented with a URL, probably `localhost:3000`. Open it in your browser and let's start.

Open `App.js` and replace its content with

```jsx
import React from 'react';

function App() {
  return <div className="App"></div>;
}

export default App;
```

You can see in the browser that the initial app was replaced with an empty file

Let's add the main ingredients for the app (it won't be beautiful, sorry)

replace `<div className="App"></div>`
with

```jsx
<div className="app">
  <div>Counter: </div>
  <div>Address: </div>
  <div>Balance: </div>
  <div>Wallet Error: </div>
  <div>Balance Error: </div>
  <div>Contract Error: </div>
  <div>Operation Error: </div>
  <button>Increase</button>
  <button>Decrease</button>
  <button>Connect</button>
</div>
```

This post was relatively short, we just bootstrapped our app. In the next post, we will connect the app to a wallet and show its balance.

For more stuff about tezos, go to our [blog](tezos.co.il)
